//
//  ViewController.swift
//  bill-splitter
//
//  Created by Indoalliz on 03/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var dinnerTextField: UITextField!
    @IBOutlet weak var totalCostTextField: UITextField!
    
    var resultText = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func dismissKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    /**
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let dinnerStr = dinnerTextField.text, let totalStr = totalCostTextField.text, let dinner = Double(dinnerStr), let total = Double(totalStr)
        {
            let formattedTotal = String(format: "%.2f", total/dinner)
            let formattedCost = String(format: "%.2f", total)

            resultText = "The total cost of food is $\(formattedCost)\n\nNumber of dinners: \(Int(dinner))\nEach dinner pays: $\(formattedTotal)"
            return true
        }
        return false
    }
 */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier
        {
            if identifier == "cheap"
            {
                let navController = segue.destination as! UINavigationController
                let destinationVC = navController.topViewController as! ResultViewController
                destinationVC.finalText = resultText
            }
            else
            {
                let destinationVC = segue.destination as! CostlyViewController
                destinationVC.finalTextCostly = resultText
                destinationVC.titleCostly = "Your dinner was expensive!"
            }
        }
        /**
        let destinationVC = segue.destination as! ResultViewController
        destinationVC.finalText = resultText
        */
    }
    
    @IBAction func calculate(_ sender: Any) {
        if let dinnerStr = dinnerTextField.text, let totalStr = totalCostTextField.text, let dinner = Double(dinnerStr), let total = Double(totalStr)
        {
            let formattedTotal = String(format: "%.2f", total/dinner)
            let formattedCost = String(format: "%.2f", total)
            
            resultText = "The total cost of food is $\(formattedCost)\n\nNumber of dinners: \(Int(dinner))\nEach dinner pays: $\(formattedTotal)"
            
            if total > 100 {
                performSegue(withIdentifier: "costly", sender: self)
            } else {
                performSegue(withIdentifier: "cheap", sender: self)
            }
        }
    }
    
    @IBAction func dismissVC(segue: UIStoryboardSegue) {
        
    }
    @IBAction func helpClicked(_ sender: AnyObject) {
        print("button")
        let helpVC = storyboard?.instantiateViewController(withIdentifier: "nav")
        present(helpVC!, animated: true, completion: nil)
    }
}

