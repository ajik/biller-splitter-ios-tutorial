//
//  ResultViewController.swift
//  bill-splitter
//
//  Created by Indoalliz on 03/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    
    @IBOutlet weak var resultLabel: UILabel!
    var finalText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultLabel.text = finalText
        
        // Do any additional setup after loading the view.
    }
    
    
    /**
    @IBAction func close(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
     */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
